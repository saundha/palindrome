package sheridan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid input", Palindrome.isPalindrome("anna") == true);
	}

	@Test
	public void testIsPalindromeNegative() {
		assertFalse("Invalid input", Palindrome.isPalindrome("sample") == true);
	}

	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid input", Palindrome.isPalindrome("race car") == true);
	}

	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("Invalid input", Palindrome.isPalindrome("race a car") == true);
	}

}
